<?php

/* HelpDeskBundle:Incidence:edit.html.twig */
class __TwigTemplate_ecb7eb74cc8fd059b0292bc5c67b5a5e2b14b5c1e2259ca409d4444c48fe1702 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_75a4d1add0dfd073615f5a5692599198948c0c0ad0ff93d052951308c94612ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_75a4d1add0dfd073615f5a5692599198948c0c0ad0ff93d052951308c94612ba->enter($__internal_75a4d1add0dfd073615f5a5692599198948c0c0ad0ff93d052951308c94612ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "HelpDeskBundle:Incidence:edit.html.twig"));

        $__internal_a33e37b60cb8c676678b71bdeadc60bf17485e2fb695c36ccff3a606b28acb57 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a33e37b60cb8c676678b71bdeadc60bf17485e2fb695c36ccff3a606b28acb57->enter($__internal_a33e37b60cb8c676678b71bdeadc60bf17485e2fb695c36ccff3a606b28acb57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "HelpDeskBundle:Incidence:edit.html.twig"));

        // line 1
        echo "<style type=\"text/css\">
    label {
        display: inline-block;
        width:125px;
        vertical-align: top;
       } 
    input, textarea {
       width:200px;
       }
    input:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
    textarea:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
</style>

<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
    </head>
    <body>
        <h1>Detail for incidence</h1>
        ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
            ";
        // line 28
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "userName", array()), 'row');
        echo "
            ";
        // line 29
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "userEmail", array()), 'row');
        echo "
            ";
        // line 30
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "cause", array()), 'row');
        echo "
            ";
        // line 31
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "description", array()), 'row');
        echo "
            ";
        // line 32
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "itsDangerouse", array()), 'row');
        echo "
            ";
        // line 33
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "save", array()), 'row');
        echo "
            <button type=\"button\"
                onclick=\"location.href='";
        // line 35
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incidenceList");
        echo "';\">return To list
            </button>
        ";
        // line 37
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
    </body>
</html>
";
        
        $__internal_75a4d1add0dfd073615f5a5692599198948c0c0ad0ff93d052951308c94612ba->leave($__internal_75a4d1add0dfd073615f5a5692599198948c0c0ad0ff93d052951308c94612ba_prof);

        
        $__internal_a33e37b60cb8c676678b71bdeadc60bf17485e2fb695c36ccff3a606b28acb57->leave($__internal_a33e37b60cb8c676678b71bdeadc60bf17485e2fb695c36ccff3a606b28acb57_prof);

    }

    public function getTemplateName()
    {
        return "HelpDeskBundle:Incidence:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 37,  82 => 35,  77 => 33,  73 => 32,  69 => 31,  65 => 30,  61 => 29,  57 => 28,  53 => 27,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<style type=\"text/css\">
    label {
        display: inline-block;
        width:125px;
        vertical-align: top;
       } 
    input, textarea {
       width:200px;
       }
    input:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
    textarea:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
</style>

<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
    </head>
    <body>
        <h1>Detail for incidence</h1>
        {{ form_start(form) }}
            {{ form_row(form.userName) }}
            {{ form_row(form.userEmail) }}
            {{ form_row(form.cause) }}
            {{ form_row(form.description) }}
            {{ form_row(form.itsDangerouse) }}
            {{ form_row(form.save) }}
            <button type=\"button\"
                onclick=\"location.href='{{ path('incidenceList') }}';\">return To list
            </button>
        {{ form_end(form) }}
    </body>
</html>
", "HelpDeskBundle:Incidence:edit.html.twig", "/home/racso/Master/mpwar1617-framework-symfony-2/src/HelpDeskBundle/Resources/views/Incidence/edit.html.twig");
    }
}
