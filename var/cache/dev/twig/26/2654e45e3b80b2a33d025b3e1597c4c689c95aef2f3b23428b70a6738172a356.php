<?php

/* HelpDeskBundle:Resolution:resolutionCreate.html.twig */
class __TwigTemplate_f81b01e8578339ab834f6a0d6053be7f85c29147dd5c62aa557e473922d7c2d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_96af1ca9895a947f7ef09c6e7c5c26125ce09044e016016bb059c6cc856cba3c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_96af1ca9895a947f7ef09c6e7c5c26125ce09044e016016bb059c6cc856cba3c->enter($__internal_96af1ca9895a947f7ef09c6e7c5c26125ce09044e016016bb059c6cc856cba3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "HelpDeskBundle:Resolution:resolutionCreate.html.twig"));

        $__internal_211697c6b30b2498b759f34e113a4b1bd95935ffb6e0f688d242b4248acb3845 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_211697c6b30b2498b759f34e113a4b1bd95935ffb6e0f688d242b4248acb3845->enter($__internal_211697c6b30b2498b759f34e113a4b1bd95935ffb6e0f688d242b4248acb3845_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "HelpDeskBundle:Resolution:resolutionCreate.html.twig"));

        // line 1
        echo "<style type=\"text/css\">
    label {
        display: inline-block;
        width:125px;
        vertical-align: top;
       } 
    input, textarea {
       width:200px;
       }
    input:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
    textarea:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
</style>

<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
    </head>
    <body>
        <h1>Create resolution</h1>
        ";
        // line 27
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
            ";
        // line 28
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "solution", array()), 'row');
        echo "
            ";
        // line 29
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "finished", array()), 'row');
        echo "
            ";
        // line 30
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "save", array()), 'row');
        echo "
            <button type=\"button\"
                onclick=\"location.href='";
        // line 32
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incidenceList");
        echo "';\">return To list
            </button>
        ";
        // line 34
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
    </body>
</html>
";
        
        $__internal_96af1ca9895a947f7ef09c6e7c5c26125ce09044e016016bb059c6cc856cba3c->leave($__internal_96af1ca9895a947f7ef09c6e7c5c26125ce09044e016016bb059c6cc856cba3c_prof);

        
        $__internal_211697c6b30b2498b759f34e113a4b1bd95935ffb6e0f688d242b4248acb3845->leave($__internal_211697c6b30b2498b759f34e113a4b1bd95935ffb6e0f688d242b4248acb3845_prof);

    }

    public function getTemplateName()
    {
        return "HelpDeskBundle:Resolution:resolutionCreate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 34,  70 => 32,  65 => 30,  61 => 29,  57 => 28,  53 => 27,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<style type=\"text/css\">
    label {
        display: inline-block;
        width:125px;
        vertical-align: top;
       } 
    input, textarea {
       width:200px;
       }
    input:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
    textarea:focus {
        border: 2px solid #000;
        background: #F3F3F3;
    }
</style>

<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
    </head>
    <body>
        <h1>Create resolution</h1>
        {{ form_start(form) }}
            {{ form_row(form.solution) }}
            {{ form_row(form.finished) }}
            {{ form_row(form.save) }}
            <button type=\"button\"
                onclick=\"location.href='{{ path('incidenceList') }}';\">return To list
            </button>
        {{ form_end(form) }}
    </body>
</html>
", "HelpDeskBundle:Resolution:resolutionCreate.html.twig", "/home/racso/Master/Frameworks/symfony-standard/src/HelpDeskBundle/Resources/views/Resolution/resolutionCreate.html.twig");
    }
}
