<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_079c43a15a78497e5681751c637615b07f0970b83341b87175cac9c67dfc99f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_28fe1ea3c6922f543e80240d6c35b4556d8a492f49fe336d89c673d1155238c8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_28fe1ea3c6922f543e80240d6c35b4556d8a492f49fe336d89c673d1155238c8->enter($__internal_28fe1ea3c6922f543e80240d6c35b4556d8a492f49fe336d89c673d1155238c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_c7702686cb8a6caf87552df4aae222e7af921d6216f98037288b9540ceafccb1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c7702686cb8a6caf87552df4aae222e7af921d6216f98037288b9540ceafccb1->enter($__internal_c7702686cb8a6caf87552df4aae222e7af921d6216f98037288b9540ceafccb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_28fe1ea3c6922f543e80240d6c35b4556d8a492f49fe336d89c673d1155238c8->leave($__internal_28fe1ea3c6922f543e80240d6c35b4556d8a492f49fe336d89c673d1155238c8_prof);

        
        $__internal_c7702686cb8a6caf87552df4aae222e7af921d6216f98037288b9540ceafccb1->leave($__internal_c7702686cb8a6caf87552df4aae222e7af921d6216f98037288b9540ceafccb1_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_dbfd769bd6372b42a62f4f940d05006cae1ddfcdd0ee3b78e4f91bda24c279bb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dbfd769bd6372b42a62f4f940d05006cae1ddfcdd0ee3b78e4f91bda24c279bb->enter($__internal_dbfd769bd6372b42a62f4f940d05006cae1ddfcdd0ee3b78e4f91bda24c279bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_c13d33c018b0e357c1b3f9ffaf2123e084152fa70bd438c0f9f1bcfcf5c91671 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c13d33c018b0e357c1b3f9ffaf2123e084152fa70bd438c0f9f1bcfcf5c91671->enter($__internal_c13d33c018b0e357c1b3f9ffaf2123e084152fa70bd438c0f9f1bcfcf5c91671_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_c13d33c018b0e357c1b3f9ffaf2123e084152fa70bd438c0f9f1bcfcf5c91671->leave($__internal_c13d33c018b0e357c1b3f9ffaf2123e084152fa70bd438c0f9f1bcfcf5c91671_prof);

        
        $__internal_dbfd769bd6372b42a62f4f940d05006cae1ddfcdd0ee3b78e4f91bda24c279bb->leave($__internal_dbfd769bd6372b42a62f4f940d05006cae1ddfcdd0ee3b78e4f91bda24c279bb_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "/home/racso/Master/Frameworks/symfony-standard/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}
