<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_57eac7e57ee9c10f4f48966119caf00ca08103952a8e037d78b3a81a68618438 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ab7331997948842ec50f5fa6178efd4e488f9daf9177aa11b4a43b940b0c7e20 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ab7331997948842ec50f5fa6178efd4e488f9daf9177aa11b4a43b940b0c7e20->enter($__internal_ab7331997948842ec50f5fa6178efd4e488f9daf9177aa11b4a43b940b0c7e20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_7bd5d3ee587e333bc3658de9be9849043d92488b797f4ad9225f2511f72ee430 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7bd5d3ee587e333bc3658de9be9849043d92488b797f4ad9225f2511f72ee430->enter($__internal_7bd5d3ee587e333bc3658de9be9849043d92488b797f4ad9225f2511f72ee430_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ab7331997948842ec50f5fa6178efd4e488f9daf9177aa11b4a43b940b0c7e20->leave($__internal_ab7331997948842ec50f5fa6178efd4e488f9daf9177aa11b4a43b940b0c7e20_prof);

        
        $__internal_7bd5d3ee587e333bc3658de9be9849043d92488b797f4ad9225f2511f72ee430->leave($__internal_7bd5d3ee587e333bc3658de9be9849043d92488b797f4ad9225f2511f72ee430_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_b5de847e1bfa018d07d5117666fa01db9a568ef600be8222ec28f037c05335a3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b5de847e1bfa018d07d5117666fa01db9a568ef600be8222ec28f037c05335a3->enter($__internal_b5de847e1bfa018d07d5117666fa01db9a568ef600be8222ec28f037c05335a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_eb1040cccb0d5b6200fad632f7476fd8002eb9d3acfddf7f3ccc4d3c52177b5e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eb1040cccb0d5b6200fad632f7476fd8002eb9d3acfddf7f3ccc4d3c52177b5e->enter($__internal_eb1040cccb0d5b6200fad632f7476fd8002eb9d3acfddf7f3ccc4d3c52177b5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_eb1040cccb0d5b6200fad632f7476fd8002eb9d3acfddf7f3ccc4d3c52177b5e->leave($__internal_eb1040cccb0d5b6200fad632f7476fd8002eb9d3acfddf7f3ccc4d3c52177b5e_prof);

        
        $__internal_b5de847e1bfa018d07d5117666fa01db9a568ef600be8222ec28f037c05335a3->leave($__internal_b5de847e1bfa018d07d5117666fa01db9a568ef600be8222ec28f037c05335a3_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_6c93883e9babd102ed39a361fa9a0c457285d6d0ecdefdb7a0c5020c3178d323 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6c93883e9babd102ed39a361fa9a0c457285d6d0ecdefdb7a0c5020c3178d323->enter($__internal_6c93883e9babd102ed39a361fa9a0c457285d6d0ecdefdb7a0c5020c3178d323_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_026e929b4157f4ddcdd58103615e990d00863eb0a93c0900779406919619cec4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_026e929b4157f4ddcdd58103615e990d00863eb0a93c0900779406919619cec4->enter($__internal_026e929b4157f4ddcdd58103615e990d00863eb0a93c0900779406919619cec4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_026e929b4157f4ddcdd58103615e990d00863eb0a93c0900779406919619cec4->leave($__internal_026e929b4157f4ddcdd58103615e990d00863eb0a93c0900779406919619cec4_prof);

        
        $__internal_6c93883e9babd102ed39a361fa9a0c457285d6d0ecdefdb7a0c5020c3178d323->leave($__internal_6c93883e9babd102ed39a361fa9a0c457285d6d0ecdefdb7a0c5020c3178d323_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_ed763724d367fa8061fbb35c78c2656131122c4250a71ba44b17a6071d8adf05 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ed763724d367fa8061fbb35c78c2656131122c4250a71ba44b17a6071d8adf05->enter($__internal_ed763724d367fa8061fbb35c78c2656131122c4250a71ba44b17a6071d8adf05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_ed6084f2afb9581e09c1fb52e4fe1ecb8312a06db4c6b0902fcdd7272285b98d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed6084f2afb9581e09c1fb52e4fe1ecb8312a06db4c6b0902fcdd7272285b98d->enter($__internal_ed6084f2afb9581e09c1fb52e4fe1ecb8312a06db4c6b0902fcdd7272285b98d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_ed6084f2afb9581e09c1fb52e4fe1ecb8312a06db4c6b0902fcdd7272285b98d->leave($__internal_ed6084f2afb9581e09c1fb52e4fe1ecb8312a06db4c6b0902fcdd7272285b98d_prof);

        
        $__internal_ed763724d367fa8061fbb35c78c2656131122c4250a71ba44b17a6071d8adf05->leave($__internal_ed763724d367fa8061fbb35c78c2656131122c4250a71ba44b17a6071d8adf05_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/racso/Master/Frameworks/symfony-standard/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
