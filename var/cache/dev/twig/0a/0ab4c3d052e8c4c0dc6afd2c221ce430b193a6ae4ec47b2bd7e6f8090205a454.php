<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_8d7851e10dd7aad790ec19aaa31cedf2ab6f56c13bfbe6354facb3527efe2e6e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7e9b8457a308559d1b54f0fa76f3baf6b21ff1b5174424b043d240c0583b03c6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7e9b8457a308559d1b54f0fa76f3baf6b21ff1b5174424b043d240c0583b03c6->enter($__internal_7e9b8457a308559d1b54f0fa76f3baf6b21ff1b5174424b043d240c0583b03c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_868b2556a05a6511cdb328d33e70ed5a9cf72a4ac94ec44d2790d1bead1be15f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_868b2556a05a6511cdb328d33e70ed5a9cf72a4ac94ec44d2790d1bead1be15f->enter($__internal_868b2556a05a6511cdb328d33e70ed5a9cf72a4ac94ec44d2790d1bead1be15f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7e9b8457a308559d1b54f0fa76f3baf6b21ff1b5174424b043d240c0583b03c6->leave($__internal_7e9b8457a308559d1b54f0fa76f3baf6b21ff1b5174424b043d240c0583b03c6_prof);

        
        $__internal_868b2556a05a6511cdb328d33e70ed5a9cf72a4ac94ec44d2790d1bead1be15f->leave($__internal_868b2556a05a6511cdb328d33e70ed5a9cf72a4ac94ec44d2790d1bead1be15f_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_6485712edb6a9db141ff67040bcab281956b227d906a03cacd1ae7afc7c3e707 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6485712edb6a9db141ff67040bcab281956b227d906a03cacd1ae7afc7c3e707->enter($__internal_6485712edb6a9db141ff67040bcab281956b227d906a03cacd1ae7afc7c3e707_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_61596afb3060b400f0f5029b3a4032b5c6978d9358e5480fd8942d8e6c8925a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_61596afb3060b400f0f5029b3a4032b5c6978d9358e5480fd8942d8e6c8925a3->enter($__internal_61596afb3060b400f0f5029b3a4032b5c6978d9358e5480fd8942d8e6c8925a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_61596afb3060b400f0f5029b3a4032b5c6978d9358e5480fd8942d8e6c8925a3->leave($__internal_61596afb3060b400f0f5029b3a4032b5c6978d9358e5480fd8942d8e6c8925a3_prof);

        
        $__internal_6485712edb6a9db141ff67040bcab281956b227d906a03cacd1ae7afc7c3e707->leave($__internal_6485712edb6a9db141ff67040bcab281956b227d906a03cacd1ae7afc7c3e707_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_d84686d89cfca3a089f77de6fcb32042ce99711350822f9ba1e850f5341725c0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d84686d89cfca3a089f77de6fcb32042ce99711350822f9ba1e850f5341725c0->enter($__internal_d84686d89cfca3a089f77de6fcb32042ce99711350822f9ba1e850f5341725c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_d7c6d8cbda675e3a466a7fefc99d53b7481a5a25127b0d5c44c15ab4107eb292 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d7c6d8cbda675e3a466a7fefc99d53b7481a5a25127b0d5c44c15ab4107eb292->enter($__internal_d7c6d8cbda675e3a466a7fefc99d53b7481a5a25127b0d5c44c15ab4107eb292_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_d7c6d8cbda675e3a466a7fefc99d53b7481a5a25127b0d5c44c15ab4107eb292->leave($__internal_d7c6d8cbda675e3a466a7fefc99d53b7481a5a25127b0d5c44c15ab4107eb292_prof);

        
        $__internal_d84686d89cfca3a089f77de6fcb32042ce99711350822f9ba1e850f5341725c0->leave($__internal_d84686d89cfca3a089f77de6fcb32042ce99711350822f9ba1e850f5341725c0_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_facab33f421bac850ee1f875b75b86d4a48b4df028ebba7fce861431c6a44966 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_facab33f421bac850ee1f875b75b86d4a48b4df028ebba7fce861431c6a44966->enter($__internal_facab33f421bac850ee1f875b75b86d4a48b4df028ebba7fce861431c6a44966_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_31858ef6b5d8690abed58b621ce4bf7f26b5d1f2c0495cc9710b8686c94a8987 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_31858ef6b5d8690abed58b621ce4bf7f26b5d1f2c0495cc9710b8686c94a8987->enter($__internal_31858ef6b5d8690abed58b621ce4bf7f26b5d1f2c0495cc9710b8686c94a8987_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_31858ef6b5d8690abed58b621ce4bf7f26b5d1f2c0495cc9710b8686c94a8987->leave($__internal_31858ef6b5d8690abed58b621ce4bf7f26b5d1f2c0495cc9710b8686c94a8987_prof);

        
        $__internal_facab33f421bac850ee1f875b75b86d4a48b4df028ebba7fce861431c6a44966->leave($__internal_facab33f421bac850ee1f875b75b86d4a48b4df028ebba7fce861431c6a44966_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/racso/Master/Frameworks/symfony-standard/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
