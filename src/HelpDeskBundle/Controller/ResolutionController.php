<?php

NAMESPACE HelpDeskBundle\Controller;

USE HelpDeskBundle\Entity\Resolution;
USE HelpDeskBundle\Domain\ResolutionDTO;
USE HelpDeskBundle\Form\Type\ResolutionType;
USE HelpDeskBundle\Service\IncidenceService;
USE HelpDeskBundle\Service\ResolutionService;
USE Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
USE Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
USE Symfony\Bundle\FrameworkBundle\Controller\Controller;
USE Symfony\Component\HttpFoundation\Request;
USE Symfony\Component\HttpFoundation\Response;
USE Symfony\Component\Form\Form;

class ResolutionController extends Controller
{
    /**
    * Se muestra detalle de resolucion y se recupera resultado de acción para guardar el registro
    * @Route("/resolution/create/{incidenceId}", name="resolutionCreate")
    * @Template
    */
    public function resolutionCreateAction(Request $request, Int $incidenceId)
    {
        $form = $this->createForm(ResolutionType::class);
        $form->handleRequest($request);

        if ($this->isFormSubmittedCorrectly($form)) {
            $resolutionDTO = $this->getResolutionToForm($form);
            $resolutionDTO->setIncidenceId($incidenceId);
            $resolutionService = $this->get("resolutionService");
            $resolutionService->create($resolutionDTO);

            return $this->redirect($this->generateUrl('incidenceList'));
        }

        return $this->render('HelpDeskBundle:Resolution:resolutionCreate.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
    * Se rellena la clase DTO con los valores obtenidos del formulario
    **/
    private function getResolutionToForm(Form $form):ResolutionDTO{
        $resolutionDTO = new ResolutionDTO();
        
        $resolutionDTO->setSolution($form->get('solution')->getData());
        $resolutionDTO->setFinished($form->get('finished')->getData());
            
        return $resolutionDTO;
    }

    /**
    * Se indica si el formulario ha sido correctamente enviado o no
    **/
    private function isFormSubmittedCorrectly(Form $form):bool{
        return $form->isSubmitted() && $form->isValid();
    }
}