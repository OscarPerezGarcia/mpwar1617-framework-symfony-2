<?php

NAMESPACE HelpDeskBundle\Controller;

USE Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
USE Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
USE Symfony\Bundle\FrameworkBundle\Controller\Controller;
USE Symfony\Component\HttpFoundation\Response;
USE Symfony\Component\HttpFoundation\Request;
USE Symfony\Component\Form\Form;
USE HelpDeskBundle\Entity\Resolution;
USE HelpDeskBundle\Form\Type\IncidenceType;
USE HelpDeskBundle\Service\IncidenceService;
USE HelpDeskBundle\Domain\IncidenceDTO;

class IncidenceController extends Controller
{
    /**
    * Muestra listado de incidecias existentes, asñi como da opción de crear nuevas o editarles
    * @Route("/helpDesk/incidenceList", name="incidenceList")
    * @Template
    */
    public function listAction(Request $request)
    {
        $incidenceService = $this->get("incidenceService");
        $incidences = $incidenceService->findAll();

        return $this->render('HelpDeskBundle:Incidence:list.html.twig', 
                array('incidences' => $incidences)
        );
    }

    /**
    * Acceso al detalle de la indicencia con el id indicado
    * @Route("/incidence/edit/{id}", name="incidenceEdit")
    * @Template
    */
    public function editAction(Request $request, int $id)
    {
        $incidenceService = $this->get("incidenceService");
        $incidence = $incidenceService->find($id);
        
        $form = $this->createForm(IncidenceType::class, $incidence);
        $form->handleRequest($request);

        if ($this->isFormSubmittedCorrectly($form)) {
            $incidenceDTO = $this->getIncidenceToForm($form);
            $incidenceDTO->setId($id);
            $incidenceService->save($incidenceDTO);

            return $this->redirect($this->generateUrl('incidenceList'));
        }

        return $this->render('HelpDeskBundle:Incidence:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }   

    /**
    * Se muestra formulario para crear una nueva incidencia, y se recoge el resultado de este
    * para añadir el registro en BD.
    * @Route("/helpDesk/incidenceCreate", name="incidenceCreate")
    * @Template
    */
    public function incidenceCreateAction(Request $request)
    {        
        $form = $this->createForm(IncidenceType::class);
        $form->handleRequest($request);

        if ($this->isFormSubmittedCorrectly($form)) {
            $incidenceDTO = $this->getIncidenceToForm($form);
            $incidenceService = $this->get("incidenceService");
            $incidenceService->create($incidenceDTO);

            return $this->redirect($this->generateUrl('incidenceList'));
        }

        return $this->render('HelpDeskBundle:Incidence:incidenceCreate.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
    * Se rellena la clase DTO con los valores obtenidos del formulario
    **/
    private function getIncidenceToForm(Form $form):IncidenceDTO{
        $incidenceDTO = new IncidenceDTO();
        
        $incidenceDTO->setUserName($form->get('userName')->getData());
        $incidenceDTO->setUserEmail($form->get('userEmail')->getData());
        $incidenceDTO->setCause($form->get('cause')->getData());
        $incidenceDTO->setDescription($form->get('description')->getData());
        $incidenceDTO->setItsDangerouse($form->get('itsDangerouse')->getData());
            
        return $incidenceDTO;
    }

    /**
    * Se indica si el formulario ha sido correctamente enviado o no
    **/
    private function isFormSubmittedCorrectly(Form $form):bool{
        return $form->isSubmitted() && $form->isValid();
    }
}