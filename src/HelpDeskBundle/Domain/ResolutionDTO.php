<?php

NAMESPACE HelpDeskBundle\Domain;
USE \DateTime;

final class ResolutionDTO{
	private $id;
	private $incidenceId;
	private $solution;
	private $finished;
	private $dateCreated;

	public function __construct(){
		$this->id = 0;
		$this->finished = false;
		$this->dateCreated = new DateTime("now");
	}

	public function getId():Int{
		return $this->id;
	}
	public function setId(Int $id){
		$this->id = $id;
	}

	public function getIncidenceId():Int{
		return $this->incidenceId;
	}
	public function setIncidenceId(Int $incidenceId){
		$this->incidenceId = $incidenceId;
	}

	public function getSolution():String{
		return $this->solution;
	}
	public function setSolution(String $solution){
		$this->solution = $solution;
	}

	public function getFinished():Bool{
		return $this->finished;
	}
	public function setFinished(Bool $finished){
		$this->finished = $finished;
	}

	public function getDateCreated():DateTime{
		return $this->dateCreated;
	}
	public function setDateCreated(DateTime $dateCreated){
		$this->dateCreated = $dateCreated;
	}
} 
