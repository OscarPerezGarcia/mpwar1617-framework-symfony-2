<?php

NAMESPACE HelpDeskBundle\Domain;
USE \DateTime;

final class IncidenceDTO{
	private $id;
	private $userName;
	private $userEmail;
	private $cause;
	private $description;
	private $itsDangerouse;
	private $finished;
	private $dateCreated;

	public function __construct(){
		$this->id = 0;
		$this->finished = false;
		$this->dateCreated = new DateTime("now");
	}

	public function getId():Int{
		return $this->id;
	}
	public function setId(Int $id){
		$this->id = $id;
	}

	public function getUserName():String{
		return $this->userName;
	}
	public function setUserName(String $userName){
		$this->userName = $userName;
	}

	public function getUserEmail():String{
		return $this->userEmail;
	}
	public function setUserEmail(String $userEmail){
		$this->userEmail = $userEmail;
	}

	public function getCause():String{
		return $this->cause;
	}
	public function setCause(String $cause){
		$this->cause = $cause;
	}

	public function getDescription():String{
		return $this->description;
	}
	public function setDescription(String $description){
		$this->description = $description;
	}

	public function getItsDangerouse():Bool{
		return $this->itsDangerouse;
	}
	public function setItsDangerouse(Bool $itsDangerouse){
		$this->itsDangerouse = $itsDangerouse;
	}

	public function getFinished():Bool{
		return $this->finished;
	}
	public function setFinished(Bool $finished){
		$this->finished = $finished;
	}

	public function getDateCreated():DateTime{
		return $this->dateCreated;
	}
	public function setDateCreated(DateTime $dateCreated){
		$this->dateCreated = $dateCreated;
	}
} 
