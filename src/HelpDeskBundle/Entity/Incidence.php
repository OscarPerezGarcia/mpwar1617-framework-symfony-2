<?php

NAMESPACE HelpDeskBundle\Entity;
USE \DateTime;
USE Doctrine\ORM\Mapping AS ORM;
USE Doctrine\Common\Collections\ArrayCollection;

/** 
* @ORM\Entity
* @ORM\Table(name="incidence")
*/
class Incidence {
	/** 
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	private $id;

	/**
     * @ORM\OneToMany(targetEntity="Resolution", mappedBy="incidence")
     */
    public $resolutions;

	/** 
	* @ORM\Column(type="string", length=255)
	*/
	private $userName;

	/** 
	* @ORM\Column(type="string", length=255)
	*/
	private $userEmail;

	/** 
	* @ORM\Column(type="string", length=255)
	*/
	private $cause;

	/** 
	* @ORM\Column(type="string", length=1000)
	*/
	private $description;

	/** 
	* @ORM\Column(type="boolean")
	*/
	private $itsDangerouse;

	/** 
	* @ORM\Column(type="boolean")
	*/
	private $finished;

	/** 
	* @ORM\Column(type="datetime")
	*/
	private $dateCreated;

	public function __construct
	(
	)
	{
		$this->userName = "";
		$this->userEmail = "";
		$this->cause = "";
		$this->description = "";
		$this->itsDangerouse = false;
		$this->finished = false;
		$this->resolutions = new ArrayCollection();
		$this->dateCreated = new DateTime("now");
	}

	static function create():Incidence{
		$incidence = new Incidence();
		return $incidence;
	}

	public function getId():Int{
		return $this->id;
	}
	public function setId(Int $id){
		$this->id = $id;
	}

	public function getUserName():String{
		return $this->userName;
	}
	public function setUserName(String $userName){
		$this->userName = $userName;
	}

	public function getUserEmail():String{
		return $this->userEmail;
	}
	public function setUserEmail(String $userEmail){
		$this->userEmail = $userEmail;
	}

	public function getCause():String{
		return $this->cause;
	}
	public function setCause(String $cause){
		$this->cause = $cause;
	}

	public function getDescription():String{
		return $this->description;
	}
	public function setDescription(String $description){
		$this->description = $description;
	}

	public function getItsDangerouse():Bool{
		return $this->itsDangerouse;
	}
	public function setItsDangerouse(Bool $itsDangerouse){
		$this->itsDangerouse = $itsDangerouse;
	}

	public function getFinished():Bool{
		return $this->finished;
	}
	public function setFinished(Bool $finished){
		$this->finished = $finished;
	}

	public function getDateCreated():DateTime{
		return $this->dateCreated;
	}
	public function setDateCreated(DateTime $dateCreated){
		$this->dateCreated = $dateCreated;
	}
	public function getDateCreatedFormated():String{
		return $this->dateCreated->format('Y-m-d H:i:s');
	}
}