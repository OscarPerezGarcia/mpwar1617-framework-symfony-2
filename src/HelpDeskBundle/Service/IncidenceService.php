<?php

NAMESPACE HelpDeskBundle\Service;
USE Doctrine\ORM\EntityManager;
USE Doctrine\ORM\EntityRepository;
USE Symfony\Component\EventDispatcher\EventDispatcherInterface;
USE HelpDeskBundle\Domain\IncidenceDTO;
USE HelpDeskBundle\Entity\Incidence;
USE HelpDeskBundle\Event\IncidenceCreatedEvent;
USE HelpDeskBundle\Event\IncidenceFinalizedEvent;

class IncidenceService{
    private $entityManager;
    private $eventDispatcher;

    public function __construct
    (
        EntityManager $entityManager,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
    * Se busca y devuelve la incidencia con el Id indicado
    **/
    public function find(Int $id):Incidence{
        $repository = $this->getRepository();
        $incidence = $repository->find($id);

        return $incidence;        
    }

    /**
    * Se buscan y devuelven todas las incidencias
    **/
    public function findAll():Array{
        $repository = $this->getRepository();
        $incidences = $repository->findAll();

        return $incidences;
    }

    /**
    * Se guarda la incidencia en BD
    **/
    public function save(IncidenceDTO $incidenceDTO){
        $incidence = $this->incedenceDTOtoEntity($incidenceDTO);
        $this->entityManager->flush();
    }

    /**
    * Se crea la incidencia en BD
    **/
    public function create(IncidenceDTO $incidenceDTO){
        $incidence = $this->incedenceDTOtoEntity($incidenceDTO);

        $this->entityManager->persist($incidence);
        $this->entityManager->flush();
        $this->eventDispatchCreateIncidence($incidence);
    }

    public function finalize(int $incidenceId){
        $repository = $this->getRepository();
        $incidence = $repository->find($incidenceId);
        $incidence->setFinished(true);
        $this->entityManager->flush();

        $this->eventDispatchFinalizedIncidence($incidence);
    }

    /**
    * Se copian los datos de la clase DTO a la Entity
    **/
    private function incedenceDTOtoEntity(IncidenceDTO $incidenceDTO):Incidence{
        $incidence = Incidence::create();

        $incidence->setId($incidenceDTO->getId());
        $incidence->setUserName($incidenceDTO->getUserName());
        $incidence->setUserEmail($incidenceDTO->getUserEmail());
        $incidence->setCause($incidenceDTO->getCause());
        $incidence->setDescription($incidenceDTO->getDescription());
        $incidence->setItsDangerouse($incidenceDTO->getItsDangerouse());
        $incidence->setFinished($incidenceDTO->getFinished());
        $incidence->setDateCreated($incidenceDTO->getDateCreated());

        return $incidence;
    }

    /**
    * Se llama a evento de creación de incidecia
    **/
    private function eventDispatchCreateIncidence(Incidence $incidence){
        $incidenceCreatedEvent = new IncidenceCreatedEvent($incidence);
        $event = $this->eventDispatcher;
        $event->dispatch('incidence_created', $incidenceCreatedEvent);
    }

    /**
    * Se lanza evento de finalizacion de incidencia.
    **/
    private function eventDispatchFinalizedIncidence(Incidence $incidence){
        $incidenceFinalizedEvent = new IncidenceFinalizedEvent($incidence);
        $event = $this->eventDispatcher;
        $event->dispatch('incidence_finalized', $incidenceFinalizedEvent);
    }

    /**
    * Se devuelve el repositorio de incidencia
    **/
    private function getRepository():EntityRepository{
        return $this->entityManager->getRepository("HelpDeskBundle:Incidence");
    }
}