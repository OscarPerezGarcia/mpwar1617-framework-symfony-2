<?php

NAMESPACE HelpDeskBundle\Service;
USE Doctrine\ORM\EntityManager;
USE Doctrine\ORM\EntityRepository;
USE HelpDeskBundle\Service\IncidenceService;
USE HelpDeskBundle\Domain\ResolutionDTO;
USE HelpDeskBundle\Entity\Incidence;
USE HelpDeskBundle\Entity\Resolution;

class ResolutionService{
    private $entityManager;
    private $eventDispatcher;
    private $incidenceService;

    public function __construct
    (
        EntityManager $entityManager,
        IncidenceService $incidenceService
    )
    {
        $this->entityManager = $entityManager;
        $this->incidenceService = $incidenceService;
    }

    /**
    * Se crea la resolucion en BD
    **/
    public function create(ResolutionDTO $resolutionDTO){
        $resolution = $this->resolutionDTOtoEntity($resolutionDTO);

        $this->entityManager->persist($resolution);
        $this->entityManager->flush();
        if ($resolution->getFinished()){
            $this->incidenceService->finalize($resolutionDTO->getIncidenceId());
        }
    }

    /**
    * Se copian los datos de la clase DTO a la Entity
    **/
    private function resolutionDTOtoEntity(ResolutionDTO $resolutionDTO):Resolution{
        $resolution = Resolution::create();

        $resolution->setId($resolutionDTO->getId());
        $resolution->setSolution($resolutionDTO->getSolution());
        $resolution->setFinished($resolutionDTO->getFinished());
        $resolution->setDateCreated($resolutionDTO->getDateCreated());
        $incidence = $this->incidenceService->find($resolutionDTO->getIncidenceId());
        $resolution->setIncidence($incidence);

        return $resolution;
    }

    /**
    * Se devuelve el repositorio de incidencia
    **/
    private function getRepository():EntityRepository{
        return $this->entityManager->getRepository("HelpDeskBundle:Resolution");
    }
}